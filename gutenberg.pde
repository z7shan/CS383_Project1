/*
 * guttenberg - demos text processing
 *   Loads text from Project Gutenberg, processes it into 
 *   individual words, and plays the words in sequence as an animation. 
 */


String[] words;
int index = 0;

void setup() {
  size(1600, 900);
  
  // you can also load from the web by giving a URL
  //String fn = "http://www.gutenberg.org/cache/epub/2489/pg2489.txt";
  //String start = "*END THE SMALL PRINT!";
  
  String fn = "type.txt";
  //String fn = "1342-0.txt";
  String start = "Produced by Anonymous Volunteers";

  println("Loading `" + fn + "'...");
  String[] lines = loadStrings(fn);
  println("  loaded " + lines.length + " lines");

  // strip out guttenberg frontmatter and endmatter
  StringBuilder s = new StringBuilder();
  boolean frontmatter = true;
  String buffer = "";
  boolean start_string = false;
  String out = "";
  for (String l : lines) {
    if(start_string){
      s.append(out + "\n");
      start_string = false;
    }
    String[] words = splitTokens(l, " ");
    for(String word : words){
      if(start_string){
        out = out + word + " ";
      }
      switch(buffer){
        case "int main(){ ":
          out = "It all starts here...";
          s.append(out + "\n");
          buffer = "";
          break;
        case "string love = ":
          out = "Create love into";
          s.append(out + "\n");
          buffer = "";
          start_string = true;
          out = word + " ";
          break;
//case "":
      }
      //s.append(l + " ");
      buffer = buffer + word + " ";
    }
  }
  String book = s.toString();

  println("  found " + book.length() + " characters in book");

  // split on whitespace to get individual words
  words = splitTokens(book, "\n");
  println("  found " + words.length + " words in book");
  
  // setup text style
  textSize(60);
  textAlign(CENTER, CENTER);
  background(0);
}


void draw() {

  //background(0);
  
  float alpha = 5; //adjustY(0, 100);
  fill(0, alpha);
  rect(0, 0, width, height);
   
  fill(255);
  try{
    text(words[index], width/2, height/2);
  } catch(Exception e) {
  }
  
  int update = 70; //int(adjustX(1, 60));
  
  if (frameCount % update == 0) {
    index++;
  }
}


// helper functions to adjust values with mouse

float adjustX(float low, float high) {
   float v = map(mouseX, 0, width - 1, low, high); 
   println("adjustX: ", v);
   return v;
}

float adjustY(float low, float high) {
   float v = map(mouseY, 0, height - 1, low, high); 
   println("adjustY: ", v);
   return v;
}