/* 
 * Face tracking
 * 
 *    Code based on demos in OpenCV for Processing 0.5.4 
 *    by Greg Borenstein http://gregborenstein.com
 */

boolean DEBUG_MODE = false;

import processing.video.*;

import gab.opencv.*;

// to get Java Rectangle type
import java.awt.*; 

Capture cam;
OpenCV opencv;
//PImage emoji;

// scale factor to downsample frame for processing 
float scale = 0.5;

// image to display
PImage output;

// array of bounding boxes for face
Rectangle[] faces;

void setup() {
  size(640, 480);

  // want video frame and opencv proccessing to same size
  cam = new Capture(this, int(640 * scale), int(480 * scale));

  opencv = new OpenCV(this, cam.width, cam.height);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  

  cam.start();

  // init to empty image
  output = new PImage(cam.width, cam.height);
}


void draw() {

  if (cam.available() == true) {
    cam.read();

    // load frame into OpenCV 
    opencv.loadImage(cam);

    // it's often useful to mirror image to make interaction easier
    // 1 = mirror image along x
    // 0 = mirror image along y
    // -1 = mirror x and y
    opencv.flip(1);

    faces = opencv.detect();

    // switch to RGB mode before we grab the image to display
    opencv.useColor(RGB);
    output = opencv.getSnapshot(); 
  }

  // draw the image
  pushMatrix();
  scale(1 / scale);
  image(output, 0, 0 );
  popMatrix();

  // draw face tracking debug
  if (faces != null) {
    int x0 = 0, y0 = 0, w0 = 0, h0 = 0;
    for (int i=0; i < faces.length; i++) {
      // scale the tracked faces to canvas size
      float s = 1 / scale;
      
      if(i==0){
        x0 = int(faces[0].x * s);
        y0 = int(faces[0].y * s);
        w0 = int(faces[0].width * s);
        h0 = int(faces[0].height * s);
      }
      
      int x = int(faces[i].x * s);
      int y = int(faces[i].y * s);
      int w = int(faces[i].width * s);
      int h = int(faces[i].height * s);

      if(i == 1){
        PImage crop = get(x,y,w,h);
        PImage crop2 = get(x0,y0,w0,h0);
        pushMatrix();
        translate(x0,y0);
        scale(float(w0)/float(w));
        image(crop, 0,0);
        popMatrix();
        
        pushMatrix();
        translate(x,y);
        scale(float(w)/float(w0));
        image(crop2, 0,0);
        popMatrix();
      }

      // draw bounding box and a "face id"
      if(DEBUG_MODE){
        stroke(255, 255, 0);
        noFill();     
        rect(x, y, w, h);
        fill(255, 255, 0);
        text(i, x, y - 20);
      }
    }

  }

  fill(255, 0, 0);
  text(nfc(frameRate, 1), 20, 20);
}