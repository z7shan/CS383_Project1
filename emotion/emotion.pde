import http.requests.*;
import java.net.URI;
import java.awt.*; 
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class Face {
  
  JSONObject obj;
  
  Face(JSONObject obj){ this.obj = obj; }
  
  public Rectangle getRect(){
    JSONObject rect = obj.getJSONObject("faceRectangle");
    int x = rect.getInt("top");
    int y = rect.getInt("left");
    int w = rect.getInt("width");
    int h = rect.getInt("height");
    return new Rectangle(x,y,w,h);
  }
  
  public double getContempt(){
    JSONObject scores = obj.getJSONObject("scores");
    return scores.getDouble("contempt");
  }
  public double getSurprise(){
    JSONObject scores = obj.getJSONObject("scores");
    return scores.getDouble("surprise");
  }
  public double getHappiness(){
    JSONObject scores = obj.getJSONObject("scores");
    return scores.getDouble("happiness");
  }
  public double getNeutral(){
    JSONObject scores = obj.getJSONObject("scores");
    return scores.getDouble("neutral");
  }
  public double getSadness(){
    JSONObject scores = obj.getJSONObject("scores");
    return scores.getDouble("sadness");
  }
  public double getDisgust(){
    JSONObject scores = obj.getJSONObject("scores");
    return scores.getDouble("disgust");
  }
  public double getAnger(){
    JSONObject scores = obj.getJSONObject("scores");
    return scores.getDouble("anger");
  }
  public double getFear(){
    JSONObject scores = obj.getJSONObject("scores");
    return scores.getDouble("fear");
  }
}

void setup() {
  String url = "https://www.girlshealth.gov/feelings/images/expressions.jpg";
  String retSrc = getEmotionResult(url);
  println(retSrc);
  printSeparateLine();
  JSONArray result = parseJSONArray(retSrc);
  println(result.toString());
  printSeparateLine();
  ArrayList<Face> faces = new ArrayList<Face>();
  for(int i=0; i<result.size(); i++){
    JSONObject scores = result.getJSONObject(i);
    //println(scores.toString());
    faces.add(new Face(scores));
  }

}

String getEmotionResult(String url) {
  HttpClient httpClient = new DefaultHttpClient();

  try
  {
    URIBuilder uriBuilder = new URIBuilder("https://westus.api.cognitive.microsoft.com/emotion/v1.0/recognize");

    URI uri = uriBuilder.build();
    HttpPost request = new HttpPost(uri);

    request.setHeader("Content-Type", "application/json");
    request.setHeader("Ocp-Apim-Subscription-Key", "94bb6a66756a4645a80e5441b6f886bd");

    // Request body. Replace the example URL below with the URL of the image you want to analyze.
    StringEntity reqEntity = new StringEntity("{ \"url\": \""+url+"\" }");
    request.setEntity(reqEntity);

    HttpResponse response = httpClient.execute(request);
    HttpEntity entity = response.getEntity();

    if (entity != null)
    {
      //println(EntityUtils.toString(entity));
      return EntityUtils.toString(entity);
    } else {
      throw new Exception("Entity is null.");
    }
  }
  catch (Exception e)
  {
    println(e.getMessage());
    return null;
  }
}

void printSeparateLine() {
  println("=======================================================================================");
}

/********************************************************
 void authentication() {
 println("POST ==================");
 String reqURL = "https://westus.api.cognitive.microsoft.com/emotion/v1.0/recognize";
 PostRequest post = new PostRequest(reqURL);
 post.addHeader("Content-Type", "application/json");
 post.addHeader("Ocp-Apim-Subscription-Key", "94bb6a66756a4645a80e5441b6f886bd");
 post.addHeader("url", "\"http://dreamicus.com/data/face/face-02.jpg\"");
 //post.addData("", "{ \"url\": \"http://dreamicus.com/data/face/face-02.jpg\" }");
 post.send();
 println("Reponse Content: " + post.getContent());
 println("Reponse Content-Length Header: " + post.getHeader("Content-Length"));
 }
 
 void getEmotion() {
 println("GET ===================");
 String reqURL = "https://westus.api.cognitive.microsoft.com/emotion/v1.0/recognize";
 GetRequest get = new GetRequest(reqURL); 
 get.addHeader("Accept", "application/json");
 get.addHeader("Ocp-Apim-Subscription-Key", "94bb6a66756a4645a80e5441b6f886bd");
 get.send();
 println("Reponse Content: " + get.getContent());
 println("Reponse Content-Length Header: " + get.getHeader("Content-Length"));
 }
 *****************************************************************/